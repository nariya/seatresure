﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData {
	public int currentQuest;
	public string currentWeapon;
	public string equippedSword;
	public string equippedBow;
	public string equippedHelm;
	public string equippedMagic;
	public int questStep;
	public int currentGold;
	public string lastScene;
	public string prevScene;
	public bool bow1Purchased;
	public bool bow2Purchased;
	public bool bow3Purchased;
	public bool bow4Purchased;
	public bool bow5Purchased;
	public bool sword1Purchased;
	public bool sword2Purchased;
	public bool sword3Purchased;
	public bool sword4Purchased;
	public bool sword5Purchased;
	public bool helm1Purchased;
	public bool helm2Purchased;
	public bool helm3Purchased;
	public bool helm4Purchased;
	public bool helm5Purchased;	
}