﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Google.Protobuf;
using Loom.Unity3d;
using Loom.Unity3d.Samples;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour {
    Identity identity;
    Contract contract;

    IAuthClient CreateAuthClient () {
        try {
            CertValidationBypass.Enable ();
            return AuthClientFactory.Configure ()
                .WithLogger (Debug.unityLogger)
                .WithClientId ("25pDQvX4O5j7wgwT052Sh3UzXVR9X6Ud") // unity3d sdk
                .WithDomain ("loomx.auth0.com")
                .WithScheme ("io.loomx.unity3d")
                .WithAudience ("https://keystore.loomx.io/")
                .WithScope ("openid profile email picture")
                .WithRedirectUrl ("http://127.0.0.1:9998/auth/auth0/")
                .Create ();
        } finally {
            CertValidationBypass.Disable ();
        }
    }

#if !UNITY_WEBGL // In WebGL all interactions with the key store should be done in the host page.
    async Task<IKeyStore> CreateKeyStore (string accessToken) {
        return await KeyStoreFactory.CreateVaultStore (new VaultStoreConfig {
            Url = "https://stage-vault.delegatecall.com/v1/",
                VaultPrefix = "unity3d-sdk",
                AccessToken = accessToken
        });
    }
#endif    

    public async void SignIn () {
#if !UNITY_WEBGL
        try {
            CertValidationBypass.Enable ();
            var authClient = this.CreateAuthClient ();
            var accessToken = await authClient.GetAccessTokenAsync ();
            var keyStore = await this.CreateKeyStore (accessToken);
            this.identity = await authClient.GetIdentityAsync (accessToken, keyStore);
            Debug.Log ("userName:" + this.identity.Username);
        } finally {
            CertValidationBypass.Disable ();
        }
#else
        var authClient = this.CreateAuthClient ();
        this.identity = await authClient.GetIdentityAsync ("", null);
#endif

        var writer = RPCClientFactory.Configure ()
            .WithLogger (Debug.unityLogger)
            .WithHTTP ("http://127.0.0.1:46658/rpc")
            //.WithWebSocket("ws://127.0.0.1:46657/websocket")
            .Create ();

        var reader = RPCClientFactory.Configure ()
            .WithLogger (Debug.unityLogger)
            .WithHTTP ("http://127.0.0.1:46658/query")
            //.WithWebSocket("ws://127.0.0.1:47000/queryws")
            .Create ();

        var client = new DAppChainClient (writer, reader) {
            Logger = Debug.unityLogger
        };
        client.TxMiddleware = new TxMiddleware (new ITxMiddlewareHandler[] {
            new NonceTxMiddleware {
                PublicKey = this.identity.PublicKey,
                    Client = client
            },
            new SignedTxMiddleware (this.identity.PrivateKey)
        });

        var contractAddr = await client.ResolveContractAddressAsync ("BluePrint");
        var callerAddr = this.identity.ToAddress ("default");
        this.contract = new Contract (client, contractAddr, callerAddr);
    }

    // Use this for initialization
    void Start () {
        Application.runInBackground = true;
        SignIn ();
    }

    // Update is called once per frame
    async void Update () {
        if (Input.GetKeyDown (KeyCode.A)) {
            SaveData save = new SaveData ();
            save.currentGold = 10000;
            SaveState (save);
            Debug.Log ("Save!!");
        }
        if (Input.GetKeyDown (KeyCode.S)) {
            var task = await LoadSaveData ();
            SaveData save = task;
            Debug.Log ("currentGold:" + save.currentGold);
        }
    }

    public async void SaveState (SaveData saveData) {
        if (this.identity == null) {
            throw new System.Exception ("Not signed in!");
        }

        // Save state to the backend
        var json = JsonConvert.SerializeObject (saveData);
        var data = new MapEntry {
            Key = "saveData",
            Value = json
        };
        await this.contract.CallAsync ("SetMsg", data);
    }

    public async Task<SaveData> LoadSaveData () {
        try {
            // NOTE: Query results can be of any type that can be deserialized via Newtonsoft.Json.
            var result = await contract.StaticCallAsync<MapEntry> ("GetMsg", new MapEntry {
                Key = "saveData"
            });

            Debug.Assert (result != null, "result is NULL!!");
            return JsonConvert.DeserializeObject<SaveData> (result.Value);
        } catch (Exception exception) {
            print (exception.Message);
            print ("Caught query exception");
            return new SaveData ();
        }
    }
}